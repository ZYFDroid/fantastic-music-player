# 音频响度扫描工具

#### 介绍
扫描程序所在目录下的所有音乐文件（.mp3, .wav)，计算响度，并保存到.loudness.cfg中（用于FantasticMusicPlayer和NightPlayer的响度均衡）

> 使用时请注意电脑散热