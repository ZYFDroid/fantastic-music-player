﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasticMusicPlayer.dbo.Model
{
    public class PlayList
    {
        private Guid Guid { get; } = Guid.NewGuid();

        public Guid GetUid() { return Guid; }

        public int ID { get; set; } = 0;
        public String Name { get; set; }

        public PlayList(string name)
        {
            Name = name;
        }
        public PlayList(string name,IEnumerable<SongEntry> songs)
        {
            Name = name;
            foreach (SongEntry item in songs)
            {
                Songs.Add(item);
            }
        }
        public virtual List<SongEntry> Songs { get; set; } = new List<SongEntry>();
 
        public override bool Equals(object obj)
        {
            return obj is PlayList list &&
                   Guid.Equals(list.Guid);
        }

        public override int GetHashCode()
        {
            return -737073652 + Guid.GetHashCode();
        }

        private float? cachedLUFS = null;

        public float GetBalancedLUFS()
        {
            if (!(cachedLUFS.HasValue))
            {
                var allLufs = Songs.Where(s => s.Loudness != null).Select(s => s.Loudness.LUFS - s.Loudness.PeakDB).ToArray();
                if (allLufs.Length == 0) return 1f;
                cachedLUFS = allLufs.Min();
                Console.WriteLine("Playlist "+Name+" has max reachable LUFS "+cachedLUFS);
            }
            return cachedLUFS.Value;
        }
    }
}
